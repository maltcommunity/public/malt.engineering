## About

Malt.engineering is build with Gridsome : https://gridsome.org/

This is a static generator that works with vue.js.
The result can then be deployed on a CDN. There is only static pages. 
It's however possible to use different sources, and even a CMS to generate the content.

## Contribute 

To run the website locally :
- npm install
- npm run develop
- go to http://localhost:8080

On a Mac with Apple Silicon, you may need to install vips to get npm install working : `brew install vips`

## Deployment

The website is deployed on every commit in master branch on cloudflare pages.

Moreover, every day the website is automatically rebuilt.
This is triggered by a cloud scheduler on Google that trigger the build on cloudflare.

See cloud scheduler :
https://console.cloud.google.com/cloudscheduler?project=malt-platform-data-production

See cloudflage pages :
https://dash.cloudflare.com/346e3c5b70226b2c28b663ac2067fb49/pages/view/malt-engineering
