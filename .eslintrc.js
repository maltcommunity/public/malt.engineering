module.exports = {
    extends: ['eslint:recommended', require.resolve('eslint-config-google'), 'plugin:gridsome/recommended', 'prettier'],
    parser: 'vue-eslint-parser',
    parserOptions: {
        sourceType: 'module',
        ecmaVersion: 2018,
    },
    plugins: ['prettier'],
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    rules: {
        'prettier/prettier': 'error',
        'require-jsdoc': 0,
    },
};
