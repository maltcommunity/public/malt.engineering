---
title:  Introduction to the Vue Composition API
link: https://www.youtube.com/watch?v=Z9gED7UkNd0
date: 2020-11-10
speakers: Nicolas Payot, Sahbi Ktifa
location: Berlin
event: Vue.js Meetup
---
