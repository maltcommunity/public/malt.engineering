---
title: How do we use Elasticsearch in production?
location: Paris
event: Elastic Paris Meetup #35
link: https://www.elastic.co/fr/videos/the-different-use-cases-of-elasticsearch-at-malt
date: 2019-01-22
speakers: Hugo Lassiège
---
