---
title: Monitor Kubernetes services with Prometheus and Thanos
link: https://www.youtube.com/watch?v=vRoLuBoRIps
date: 2020-04-15
speakers: Thierry Sallé
location: Online
event: Malt Academy
---
