---
title: "Live Coding with Vue.js: let's build a Tic-Tac-Toe game"
link: https://www.youtube.com/watch?v=un8dQjSSmxU
date: 2020-03-20
speakers: Nicolas Payot, Sahbi Ktifa
location: Paris
event: Malt Academy
---
