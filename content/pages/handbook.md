---
title: Handbook
subtitle: Handbook for new Malters
sidebar: true
---


## About

This handbook is a guide to every new Malter joining the company in the product team.
It is not a complete guide where you'll have a description on how to setup your laptop or the internal HR process. To do that, we use Notion internally.
You may wonder why it is public? Well, we value transparency and we share our practices regularly on our [blog](/blog). 
So it seems natural to us to make those information public if ever it could help other people.  


### How to contribute

All the content is [available in Gitlab](https://gitlab.com/maltcommunity/static/malt.io/-/tree/master/content/pages) and you can contribute to the project by creating a merge request.


## Introduction

In 2013, Vincent, Hugo and Jean-Baptiste created Malt (named Hopwork at this time) to connect businesses and freelancers. 
They intended to create a product driven company that will shape the future of work. 
Today, Malt is used by hundreds of thousands of companies and freelancers across Europe and its mission is to create a world where everyone is free to choose the best people to work with.
To meet these challenges, our product team must share the same values and principles and that’s the goal of this handbook. 
We detail in this book our vision for the Product team and the organization that will allow us to give every user the power of choice.
As Malt continues to grow, we hope that this handbook will be read by every new Malter in the team. 
Because ultimately, if you read this, what matters most is your talent and your ideas.
It’s only the beginning…


## Table of contents

* [Values & Culture](/pages/values-culture)
* [Organization](/pages/organization)
* [People Management & Development](/pages/people-management-development)

