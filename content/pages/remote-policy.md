---
title: Remote Policy
subtitle: You have freedom over when, where and how you work
sidebar: true
breadcrumbs:
  - label: "Life At Malt"
    url: "/careers"
--- 

Our history was built on a flexible, **hybrid** approach to home office. We believe <ins>our future is too</ins>.

We mix remote work with local work because we know there is no _“one size fits all rule”_. 
Like our community of freelancers, we believe in the power of choice. We rely on every Malters to make the right choices based on what is best for Malt. 
And we keep in mind that every person’s job and personal circumstances are different.

Some roles require more office presence, and some don't. 
For example the product team is split between Lyon, Paris, Berlin and many other cities and we feed on different cultures to create our products.
In this configuration we benefit from working from home more often... After all, there’s not much point coming to the office if you’re going to spend all day on Zoom.

On top of that, we also think that we should be able to work with the best people wherever they are.

## Working from home can be beautiful

Malters tell us that most of the time they enjoy coming to the office. We have worked hard to create pleasant offices where people can meet up, be stimulated and move their projects forward in comfort. But some days people prefer (or need) to work from home. Consider these different scenarios:

* You need to focus on a specific task and the office environment, with all its distractions, is not ideal. Working from home gives you the time (and peace) you need to concentrate.
* You are a bit sick, not so sick that you need a doctor's note and a day off, but sick enough that other people don’t want your germs. Working from home means you can avoid infecting your colleagues.
* You are tired of public transport (maybe there is a strike) and staying home will allow you to have more energy for the day's tasks.
* You are expecting a delivery but you don't know when it will arrive. Working from home means you can be there without having to take a day off.

To succeed in remote work, we use tools like slack, zoom and notion, and rituals (morning check-ins) to maintain connections and effectively collaborate with each other.
We develop a strong culture of writing.

## Meet each other stimulates our creativity

Even if we work a lot from home, we also know that our creativity is boosted when we meet each other. We benefit a lot from serendipitous encounters.
That's why we organize on a regular basis meetups, events and offsite and we invest a lot to have pleasant offices. 

Joining Malt also means that you will have regular physical meetings.

In the past, we have travelled to Lyon, Porto, Madrid, Barcelona, Agadir, Paris and many other cities to create special moments, and many features on Malt were created during unplanned workshops during those offsites.

## FAQ

### Can I come to the office every day?

Sure! There is no limit to how often you can come to the office, there will always be a place for everyone.

### Can I work from anywhere?

No, we believe that we should be able to see each other from time to time and we only recruit in countries where we operate so that we have the opportunity to meet up.
That being said, we are present in France, Spain, Germany, Netherlands and Belgium. There are a lot of possibilities.

### Can I benefit from a coworking space?

Yes, if you are in a city where there is no Malt office, we can offer you a coworking space.
In fact, this is how we started the offices in Lyon, Bordeaux or Madrid for example.





