---
title: Values & Culture
subtitle: 
sidebar: true
breadcrumbs:
    - label: "Handbook"
      url: "/pages/handbook"
---

Our core values are Malt’s culture pillars:
* It takes a community
* Ambition is the way
* Own your choices
* Joy is our secret sauce

The product team members should always be active participants to promote and apply Malt’s culture.
However, culture can be hard sometimes to understand. Culture is somehow what happens when the doors are closed. That's the cursor you use in every situation to solve it.

So, we decided to illustrate every culture pillar with real life examples.

## It takes a community  &  Joy is our secret sauce

Those two values are inseparable. We know that building Malt requires a lot of diverse talents in every field. But it's not only about individual talents, the thing is, without good communication and collaboration we can't succeed.

Here are the concrete actions we have to illustrate those values:

### team feedbacks
We know that building and keeping up to date a healthy culture requires us to introspect ourselves frequently. To do that, we need feedback from everyone. We use an application that asks for feedback every week on different topics (recognition, Alignement, Satisfaction, Wellness etc...).
On top of that, everyone is able to give feedback, anonymously or not. And all those comments will eventually have an answer, either on the application itself or in a team meeting if we think more people have the same question.

### peer reviews
Giving peer feedback is hard, but it's extremely valuable. We organize on a regular basis a session of peer feedback where people are invited to share some thoughts about someone.
But it's not only during this process, everytime you think you can help someone by giving them feedback, you have to do it.
For that, you have to follow the Netflix framework :
- Aim to assist : it has to be constructive, a feedback is not here to expel your frustration
- Actionnable : it has to be realistic. You should bring some propositions on the table
- Appreciate : feedback is a gift. You should appreciate to receive one
- Accept or discard : at the end, you decide what to do with this feedback

### regular offsites

Even if we work from different cities, in order to create a strong link between people, to help everyone understand the job of the others and develop empathy, we organize regular offsites and workshops.
We know the future is something hybrid between offices and remote and we want to keep the best of both.


### recruitment

Recruitment is key and in the hiring process, there are several steps where you'll be involved.
It's your responsibility because at the end, you'll work with these people so you have to contribute to build this community.
And one major criteria you should apply is "no jerk policy".
If you think someone can be toxic to a team or to diversity, please raise your hand. You are the gatekeeper of our culture.



## Ambition is the way & Own your choices

We could only say that Malt growth speaks for itself to illustrate ambition. But that won’t be enough.
Concretely, we act every day to cultivate and foster ambition.
Here is how:

### Empowering organization
You'll have more details about this in the following chapters. To summarize, every squad is empowered with a mission and the general context. They lead their own initiatives and they have a test and learn approach led by discovery in order to create impact.
We use OKR to give the context and the success criteria. But initiatives are led bottom up by the squads.
Autonomy is key but it also means that ownership is key. Without ownership, autonomy is an empty word.
Failure is ok. But lack of ownership is not.

### Transparency
Every Malter has access to the Data. We know that autonomy can't work without context. You will have the context in order to make informed decisions.
If you connect to our BI tools, you'll be able to know the GMV, the clients, the conversion rates etc... In every meeting you'll have sensitive information about the health of the company and the strategy.
It's however easy to drown in it.
"With great power comes great responsibility" as they said. 
