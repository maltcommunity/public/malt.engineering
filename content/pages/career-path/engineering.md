---
title: Engineering
subtitle: Career Path Engineering
sidebar: true
breadcrumbs:
    - label: "Handbook"
      url: "/pages/handbook"
    - label: "People Management & Development"
      url: "/pages/people-management-development"
---


This document describes the career path of Malt’s engineering team. The purpose of this career path is to formalise what is expected from software engineers at the different levels of their career, what they can do to level up and the different options they have to evolve at Malt.


## How it works

In order to assess individual performances, we rely on a set of clear expectations we hold about individuals: it covers how they master different expertise areas (hard skills), how they behave (soft skills) and what is their impact on the product and the company.

![alt_text](./images/images2.png "Career path overview")


### Ladders and steps

The engineering career path is composed of 3 ladders: individual contributor, leader and manager. A ladder is a linear path of career progression that is followed by employees during their career at Malt. Switching from a ladder to another is possible under the conditions described later in this document.

A ladder is composed of several steps. Each step is defined by a title (junior, senior, intermediate, staff…) and a clear description of the employee’s **expected impact**.

To be more precise, this impact is described as a list of general expectations the employee has to meet continuously to be successful at this step. These expectations are general enough to be applicable to any role in the engineering organisation and require both hard and soft skills.


### Impact

Being impact oriented is one of our mantras in Malt’s engineering team. It means to be focused on outcome (results) more than output (projects, technology, deadlines).

Software engineers’ influence area can be seens as a set of concentric circles, centred on the person. Progressing in this career path means improving impact’s intensity as well as extending this area.

As individual contributors, software engineers start working for themselves (junior) and progressively start becoming good (intermediate) and major (senior) players for their team.


![alt_text](./images/impact-reach.jpg "Impact and reach")



### Expertise

Building a product like Malt requires people mastering different specific technical areas, going from frontend development to cloud operations or data engineering. Mastering all these areas in depth is not a goal for a single person. Instead, software engineers will usually start with a strong technical area they will develop during their career and gradually explore additional areas while keeping on developing this strong primary area of expertise.

At Malt, we value people with strong technical skills, and having a primary area of expertise is more than expected of software engineers. It defines the role they will play in their team (being a frontend developer in the freelancer tribe for example) or will potentially define the team they belong to (being a data engineer in the data team). It will also set the expectations we can have towards people - basically don’t expect a data engineer to quickly create a super fancy landing page ;-)

The primary area of expertise of software engineers define their expertise in the organisation. The table below lists these expertises and associated technical skills. For each of them, specific expectations at the different steps of the career path will be detailed later.


<table>
  <tr>
   <td>Expertise
   </td>
   <td>Description
   </td>
   <td>Technical skills (examples)
   </td>
  </tr>
  <tr>
   <td>Back-end engineer
   </td>
   <td>Create services & server-side applications
   </td>
   <td>
<ul>

<li>java & kotlin on Spring Boot,

<li>operational data management,

<li>interprocess communication (sync & async),

<li>3rd party services integration,

<li>associated software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Front-end engineer
   </td>
   <td>Create web-based content and interfaces
   </td>
   <td>
<ul>

<li>server side rendering (JSP),

<li>single page applications (JS and TS with vue.js),

<li>html, css & preprocessors

<li>associated software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Mobile engineer
   </td>
   <td>Create mobile applications
   </td>
   <td>
<ul>

<li>Swift for iOS,

<li>Kotlin for Android,

<li>associated services,

<li>associated software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Cloud engineer
   </td>
   <td>Manage Cloud infrastructures
   </td>
   <td>
<ul>

<li>Google Cloud Platform,

<li>Kubernetes,

<li>Terraform,

<li>Operational databases,

<li>Monitoring & alerting systems

<li>associated software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Data engineer
   </td>
   <td>Create data pipelines to feed a Cloud data lake
   </td>
   <td>
<ul>

<li>Python and/or Spark for data engineering,

<li>Kafka, Bigquery

<li>Airflow & associated software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Data scientist
   </td>
   <td>Create prediction models and expose their results as services
   </td>
   <td>
<ul>

<li>Python,

<li>Mlflow, pandas,

<li>django

<li>associate software factory
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Product data analyst
   </td>
   <td>Create insights, reports and dashboards to drive the product development 
   </td>
   <td>
<ul>

<li>Python

<li>Looker/Dataviz

<li>Husprey

<li>SQL
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Release engineer
   </td>
   <td>Manage a software factory
   </td>
   <td>
<ul>

<li>CI/CD tools and configuration

<li>Monitoring & alerting systems

<li>Build tools for different parts of the stack

<li>Developer experience
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Security engineer
   </td>
   <td>Set up tools and processes to secure the product
   </td>
   <td>
<ul>

<li>Threat prevention, detection, analysis & response

<li>Associated tools and solutions

<li>Code analysis and reviews
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Engineering manager
   </td>
   <td>Help engineers perform and grow
   </td>
   <td>
<ul>

<li>Engineering individual & collective performance

<li>Individual feedback

<li>Career management

<li>Hiring
</li>
</ul>
   </td>
  </tr>
</table>


At some steps in the engineering career path, it is expected that people become knowledgeable outside their primary area of expertise and gain expertise in different related areas.


### Step + Expertise = Job

The combination of a step (e.g. senior) and an expertise (e.g. frontend engineer) makes a job (senior frontend engineer). This combination determines the expectations we hold on engineering team members and are used both to assess individual performance and hire new people.


### Steps, levels, and salary

Each step of each ladder is mapped to a level from 1 to 11, which determines a range in the salary grid. It is totally possible that two steps from two different ladders are mapped to the same level.


### Career evolutions & progression

Career evolution is the process of changing jobs within the organisation.

3 cases are covered:



* **Stepping up within the same ladder**: progression from one step to the next one is not automatic. An employee may only reach the next step when all the expectations of the current step are continuously met during a given period of time, and when some expectations of the next step are at least partially met. Some steps may also be reached after a minimal experience in the previous one.
* **Switching to a different ladder**: at some specific steps of a ladder, it is possible to switch to a different ladder. Ladder transitions are challenging! They require the help of management, HR and/or a specific coaching and mentoring.
* **Changing expertise**: a software engineer can expect to focus on a different area of expertise (for example, a backend engineer wants to switch to data engineering). This transition does not involve any progression in the career path but an expertise change and probably a team change. So, there is no associated ladder or step change and consequently no salary change.


### Organisational roles vs levels

Malt’s engineering team’s internal organisation defines 3 specific roles (or labels) in the engineering team: tech lead, guild lead and head of.

These roles are not described in this document as they are not considered as levels in the career path per-se.

The main reasons are:



* It wouldn’t scale as we can have only one tech lead per squad and one guild lead per guild, and one head of per matter so it would limit the number of people that could be promoted to some levels due to organisational constraints
* It would narrow engineering leaders’ role to very specific positions in the organisation
* It would prevent from sharing this role with someone else or set-up round robins within teams to take these roles
* It would go against the philosophy of these roles (help group of people).

However, as mentioned later in the document, taking one of these roles and being successful at it is a prerequisite to enter the engineering leader ladder.


### Some obvious points that are worth mentioning

The expectations listed for each level are tasks that engineers must achieve, or behaviours that engineers must adopt while doing their day-to-day job in their team. So this list of expectations should not prevent engineers from doing their duty or what is best for their team.



* Being a senior engineer does not mean ignoring “trivial” tasks and only focusing on designing more complex tasks.
* On the other hand, being an Intermediate Software Engineer 2 should not prevent you from investigating complex performance issues.

Finally, expectations are cumulative within a ladder: someone being on level X is expected to meet level X expectations and all expectations of previous levels.


## The “individual contributor” ladder

In the engineering team, the main mission of individual contributors is to design and deliver software in order to solve business problems in the scope of a team / squad / tribe.


![alt_text](./images/IC-senior2.png "IC career path until Senior 2")


To achieve this goal of solving business problems, regardless of their area of expertise, software engineers have to demonstrate their skills in the following areas:


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Software factory
   </td>
   <td>Mastery of tools and solutions involved in building the product from the developers’ laptop to the CI / CD pipeline.
   </td>
  </tr>
  <tr>
   <td>Technical architecture
   </td>
   <td>From programming language to in-depth mastery of third party components involved in the product.
   </td>
  </tr>
  <tr>
   <td>Quality
   </td>
   <td>From unit tests to reliability, scalability and user experience. Mastery of tools and techniques to maximize perceived quality.
   </td>
  </tr>
  <tr>
   <td>Platform and Product
   </td>
   <td>Product’s functional architecture (domains, workflows …), product KPIs 
   </td>
  </tr>
  <tr>
   <td>Business
   </td>
   <td>How the product contributes to Malt’s growth and success, market knowledge and product marketing, personas knowledge, product operationalisation.
   </td>
  </tr>
  <tr>
   <td>Team work
   </td>
   <td>Methodology, collaboration, communication, team work, soft skills
   </td>
  </tr>
</table>


Until the Senior 1 step, software engineers focus mainly on “how to achieve something” efficiently and autonomously, adopting a software crafts·wo·man attitude.

From Senior 1, software engineers are expected to be autonomous solving problems of arbitrary complexity. They should progressively focus less on how to solve problems and more on solving the right problems.


### Junior Software Engineer

Junior Software Engineers begin their career at Malt. They just completed a master degree in software engineering or equivalent (including internship) and joined Malt for their first work experience as software engineers.

During their journey, Junior Software Engineer are expected to evolve and eventually constantly reach the following expectations:



> General expectations: Learn to become a contributor for their team




<table>
  <tr>
   <td>Area
   </td>
   <td>Achievements
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Software factory
   </td>
   <td>Are autonomous with their local development environment (operating system, git, command line tools, IDE, docker). Occasionally require help from other team members when something goes wrong locally.
   </td>
  </tr>
  <tr>
   <td>Understand the basics of our CI / CD system. Apply all best practices to avoid breaking it.
   </td>
  </tr>
  <tr>
   <td>Provide all required information to ensure their code changes are auditable and easily understandable by their teammates (well formed git messages, details in pull requests, ...)
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Technical architecture
   </td>
   <td>Write correct and good quality code. Reach a good mastery of the programming languages they use (Java, Kotlin, JavaScript, Typescript, CSS, Python, Scala, SQL ...) and follow Malt’s formatting and coding style guide.
   </td>
  </tr>
  <tr>
   <td>Can list the 3rd party frameworks we use at Malt (Spring, vue.js …) explain what they do and roughly how they work.
   </td>
  </tr>
  <tr>
   <td>Can list the 3rd party technical components (middlewares) of Malt’s architecture (databases, messaging brokers, email services …), explain what they do and roughly how they work.
   </td>
  </tr>
  <tr>
   <td>Follow design suggestions of  more senior engineers. Understand and apply suggested changes during code reviews.
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Quality
   </td>
   <td>Are proactive in detecting and fixing quality issues and regressions they create.
   </td>
  </tr>
  <tr>
   <td>Able to implement unit and acceptance tests either autonomously or following their teammates suggestions.
   </td>
  </tr>
  <tr>
   <td>Can list the production monitoring tools in place and identify from where corresponding alerts are issued.
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Platform and Product
   </td>
   <td>Implement trivial tasks in autonomy and other tasks with the help of more experienced engineers and with detailed specifications
   </td>
  </tr>
  <tr>
   <td>Ramp-up on the domain(s) and workflow(s) they contribute to
   </td>
  </tr>
  <tr>
   <td>Participate to support resolution on the components they previously worked on
   </td>
  </tr>
  <tr>
   <td>Have a good overview of the whole product. Can identify the right team / persons to communicate with in case of issue on the product
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Business
   </td>
   <td>Can clearly explain what is Malt, how it works and what the benefits of using Malt for both clients and freelancers
   </td>
  </tr>
  <tr>
   <td>Can list the OKRs their team work on, and explain roughly why these OKRs are important for the company
   </td>
  </tr>
  <tr>
   <td>Can identify the business partners of their team and their role in the company
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Team work
   </td>
   <td>Adopt a continuous improvement mindset. Learn from others. Ask for feedback
   </td>
  </tr>
  <tr>
   <td>Communicate clearly the status of their tasks to their teammates and detect when they require help from more experienced people
   </td>
  </tr>
  <tr>
   <td>Follow their team organisation and internal processes
   </td>
  </tr>
</table>


Junior Software Engineers come to Malt with no strong area of expertise but probably some areas of interest. Their role is defined by staffing needs and their areas of interest.


### Intermediate Software engineer 1

Software Engineers who successfully completed a first work experience either at Malt or other companies.

When hired externally, this first experience (2-4 years) should be in software engineering teams and candidates should understand how this experience matches / doesn’t match what we have at Malt. This first experience should bring something in their ramp-up in their new position at Malt.

When promoted internally, they should have spent about 1 successful year as Junior Software Engineer and should already achieve some Intermediate Software Engineer 1 level expectations.


>  General expectations: Become good contributors for their team by completing average-complexity tasks as long as these tasks are well defined and well anticipated.

<table>
  <tr>
   <td>Area
   </td>
   <td>Achievements
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Software factory
   </td>
   <td>Reach an advanced mastery of their local development environment (IDE shortcuts, local environment troubleshooting).
<p>
Rarely require help from other team members when something doesn’t work locally.
   </td>
  </tr>
  <tr>
   <td>Good knowledge of git (how it works) and associated best practices to create clean pull requests.
   </td>
  </tr>
  <tr>
   <td>Understand and use the CI / CD tools and be proactive to repair builds when broken. Occasionally require help from other team members to repair what they broke.
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Technical architecture
   </td>
   <td>Start applying relevant design patterns and design principles proactively to create more concise and maintainable code. Still benefit from code reviews in this area.
   </td>
  </tr>
  <tr>
   <td>Reach a good understanding of the most important frameworks we use at Malt (Spring, vue.js, mobile SDKs, Spark …), can use them on a daily basis in an appropriate way
   </td>
  </tr>
  <tr>
   <td>Start mastering the 3rd party technical components we use at Malt
   </td>
  </tr>
  <tr>
   <td>Know the product architecture in the scope of their team and are able to present it to newcomers. Start being autonomous in at least one large area of the codebase
   </td>
  </tr>
  <tr>
   <td>Understand and start applying autonomously our best practices to deliver changes in production with 0 downtime (feature flags, database schema management, data migration, API change management ...)
   </td>
  </tr>
  <tr>
   <td>Start thinking proactively about production follow-up when writing code (valuable log messages, activity follow-up …)
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Quality
   </td>
   <td>Deliver solutions that comply with original expectation / specifications / design
   </td>
  </tr>
  <tr>
   <td>Deliver changes to production with minimal impediments
   </td>
  </tr>
  <tr>
   <td>Start implementing autonomously strong unit and acceptance tests, choosing the right strategy
   </td>
  </tr>
  <tr>
   <td>Start analysing and fixing QoS issues detected by the monitoring systems in place with the help of more senior teammates
   </td>
  </tr>
  <tr>
   <td>Are careful to do not damage code quality indicators when they contribute to the product
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Platform and Product
   </td>
   <td>Implement average-complexity tasks in autonomy as long as they are well defined and anticipated
   </td>
  </tr>
  <tr>
   <td>Are able to identify when tasks need more information (ask relevant questions, start measuring uncertainty)
   </td>
  </tr>
  <tr>
   <td>Answer basic questions about their team's functional scope
   </td>
  </tr>
  <tr>
   <td>Complete usual support tasks with the help of their teammates and progressively become autonomous
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Business
   </td>
   <td>Can list Malt's major competitors and what are Malt's key differentiators
   </td>
  </tr>
  <tr>
   <td>Have a clear understanding of how their work contribute to the product OKRs
   </td>
  </tr>
  <tr>
   <td>Can list major users segments and their specificities
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Team work
   </td>
   <td>Communicate accurately with their teammates. Raise concerns and issues as soon as they occur. Do not create tunnel effects on their tasks
   </td>
  </tr>
  <tr>
   <td>Start actively participating to their team's continuous improvement and share accurate issues / pain points with their team
   </td>
  </tr>
  <tr>
   <td>Proactively commit to take some action points to contribute to their team's continuous improvement. Act with ownership on these tasks
   </td>
  </tr>
</table>


At this step, software engineers have a strong area of expertise defining their role in the organisation and they keep on investing in this area of expertise to continuously improve.


### Intermediate Software engineer 2

This level is a milestone for Software Engineers I towards the Senior Software Engineer level. It can be reached after 1,5 or 2 years as a successful Intermediate Software Engineer 1.

For external hiring, this level is appropriate for candidates with 4-5 years experience in one or several software engineering teams and who do not yet meet the senior software engineer level.


> General expectations: Become major contributors to their team by completing tasks efficiently and in autonomy. Constantly follow best practices.


<table>
  <tr>
   <td>Area
   </td>
   <td>Achievements
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Software factory
   </td>
   <td>Good knowledge on every tool we use on our stack, able to install them separately and know what to do when something doesn't work locally
   </td>
  </tr>
  <tr>
   <td>Can identify the root cause of build failures even when caused by someone else and suggest a change. Rarely break the CI / CD system
   </td>
  </tr>
  <tr>
   <td>Can bootstrap a new module (library, service or application) in the code base with the help of someone else and make the appropriate changes in our CI / CD system
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Technical architecture
   </td>
   <td>Generally write concise and maintainable code. Their pull requests rarely need large rework due to inappropriate coding style and code quality
   </td>
  </tr>
  <tr>
   <td>Follow our codebase general organization by putting new code at the right place (shared library vs domain library vs application) and challenge their teammates who don't do it
   </td>
  </tr>
  <tr>
   <td>Think proactively about production follow-up when writing code (valuable log messages, activity follow-up …) and can deliver any change to production with 0 downtime
   </td>
  </tr>
  <tr>
   <td>Apply most common best practices on cyber-security proactively. Can still benefit from code reviews on this topic.
   </td>
  </tr>
  <tr>
   <td>Are fully autonomous in one large area of the codebase
   </td>
  </tr>
  <tr>
   <td>Start troubleshooting performance and scalability issues with the help of more experienced teammates
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Quality
   </td>
   <td>Constantly deliver solutions with few / no defects with regards to the original expectations / specifications / design
   </td>
  </tr>
  <tr>
   <td>Start anticipating unpredicted corner cases and impacts
   </td>
  </tr>
  <tr>
   <td>Constantly implement strong unit and acceptance tests autonomously.
   </td>
  </tr>
  <tr>
   <td>Often analyse and fix QoS issues detected by the monitoring systems
   </td>
  </tr>
  <tr>
   <td>Rarely damage code quality indicators and proactively fix their unexpected impact on them
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Platform and Product
   </td>
   <td>Implement average-complexity tasks in autonomy with minimal details
   </td>
  </tr>
  <tr>
   <td>Start designing complex tasks with guidance and reviews from more senior teammates
   </td>
  </tr>
  <tr>
   <td>Complete usual support tasks in autonomy
   </td>
  </tr>
  <tr>
   <td>Can list the main product success metrics of their team, explain how they are computed in detail
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Business
   </td>
   <td>Develop their empathy towards end users, understand what they expect from Malt. Have already met or interviewed freelancers and / or clients.
   </td>
  </tr>
  <tr>
   <td>Understand how their business partners work. Have already spent several days shadowing business teams
   </td>
  </tr>
  <tr>
   <td>Can list most important product control KPIs and how to follow them
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Team work
   </td>
   <td>Provide reliable complexity / duration estimates for average tasks of their team
   </td>
  </tr>
  <tr>
   <td>Participate to a guild and benefit from other members knowledge
   </td>
  </tr>
  <tr>
   <td>Occasionally organise team events / meetings.
   </td>
  </tr>
  <tr>
   <td>Can deliver a good quality demonstration of their work
   </td>
  </tr>
</table>



### Senior Software engineer 1

At this level, software engineers have reached a good level of maturity regarding how things are done and progressively focus more on what is the best thing to do.

When hired externally, senior software engineers are expected to have at least 6 years experience in different software engineering teams, and have probably already been promoted to senior or lead developer in a previous company.


> General expectations: act as a software crafts·wo·man and consistently design and build high quality and reliable solutions efficiently


<table>
  <tr>
   <td>Area
   </td>
   <td>Achievements
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Software factory
   </td>
   <td>Are fully productive with their local development environment giving their teammates the feeling they work fast.
<p>
Are able to help more junior members to troubleshoot the issues they have on their local development environment and giving them advice to be more productive
   </td>
  </tr>
  <tr>
   <td>Can bootstrap a new module in the code base fully autonomously and make the appropriate changes in our CI / CD system
   </td>
  </tr>
  <tr>
   <td>Can support their team's production alone during a short period (like holidays). Can deliver any emergency fix to production. Can overcome any CI / CD issue that would prevent to build and deliver the product to production
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Technical architecture
   </td>
   <td>Constantly follow SOLID principles and write concise, maintainable and reliable code
   </td>
  </tr>
  <tr>
   <td>Make well-reasoned and pragmatic design decisions and tradeoffs
   </td>
  </tr>
  <tr>
   <td>Master most of the 3rd party technical components we use at Malt and how they are integrated
   </td>
  </tr>
  <tr>
   <td>Constantly deliver valuable code reviews to their teammates and can clearly explain the benefits of proposed changes
   </td>
  </tr>
  <tr>
   <td>Analyse and fix performance or scalability issues. Detect potential performance or scalability issues on existing code. Anticipate performance or scalability issues when designing and implementing solutions.
   </td>
  </tr>
  <tr>
   <td>Are fully autonomous in at least one large area of the codebase
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Quality
   </td>
   <td>Help less experienced teammates to elaborate strong automated tests. Take a critical look at the strength or stability of unit / acceptance tests and propose the appropriate improvements
   </td>
  </tr>
  <tr>
   <td>Can elaborate a regression test strategy to secure updates on sensible parts of the product.
   </td>
  </tr>
  <tr>
   <td>Can set the right severity of a QoS issue and decide to either ignore it or invest time in fixing it
   </td>
  </tr>
  <tr>
   <td>Leverage their experience to anticipate QoS issues and take the appropriate decisions.
   </td>
  </tr>
  <tr>
   <td>Actively participate in critical production issues resolution and be proactive in post-mortems.
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Platform and Product
   </td>
   <td>Implement complex tasks in autonomy, being able to break them down in smaller subtasks with a strategy to continuously deliver the result to production as soon as completed
   </td>
  </tr>
  <tr>
   <td>Start elaborating countermeasures on recurring support tickets
   </td>
  </tr>
  <tr>
   <td>Start taking a critical look at functional requirements, business teams needs, and support tickets.
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Business
   </td>
   <td>Start participating in identifying solutions to meet product OKRs
   </td>
  </tr>
  <tr>
   <td>Participate in product adoption in business teams
   </td>
  </tr>
  <tr>
   <td>Can have a discussion about the company key results and its strategy
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Team work
   </td>
   <td>Ensure best practices are followed by others, challenge people to do their best while accepting to be challenged by others
   </td>
  </tr>
  <tr>
   <td>Turn failures into knowledge, formalise and share learnings. Proactively create documentation, Malt Tech Training Sessions and actionable post-mortem
   </td>
  </tr>
  <tr>
   <td>Are active guild contributors by bringing new knowledge, carrying out guilds initiatives in their team
   </td>
  </tr>
  <tr>
   <td>Conduct an active technological watch and sometimes bring new practices / technology to their team.
   </td>
  </tr>
  <tr>
   <td>Can organise their team work for a short period (holidays)
   </td>
  </tr>
</table>


Senior software engineers are expected to become technical experts in their major area of expertise and develop their skills in other disciplines.


### Senior Software engineer 2

This level is reached by senior software engineers 1 who have consistently demonstrated strong ownership on both functional and technical aspects of the product during 2 years.

At this step, senior software engineers are seen as experts in their team and in the guild(s) they participate in.

When hired externally, senior software engineers 2 are expected to have at least 8 years experience in different software engineering teams, and have probably already been promoted to senior or lead developer in a previous company. They expect to have previous experiences in a marketplace or in a tech company. During interviews, we’ll expect good discussions on business, product and engineering strategy and deep technical expertise.

Senior Software Engineer 2 are expected to:


<table>
  <tr>
   <td>Area
   </td>
   <td>Achievements
   </td>
  </tr>
  <tr>
   <td>Software factory
   </td>
   <td>Could be their team’s point person on CI / CD and build tools
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Technical architecture
   </td>
   <td>Master several large areas of the codebase. They are able to think strategically about the future of the codebase.
   </td>
  </tr>
  <tr>
   <td>Master most of the 3rd party technical components we use at Malt. Are able to upgrade them when necessary with well managed impacts for their teammates
   </td>
  </tr>
  <tr>
   <td>Find the root cause or a quick workaround to most of the technical issues on the product
   </td>
  </tr>
  <tr>
   <td rowspan="2" >Quality
   </td>
   <td>Elaborate automated tests strategies to monitor quality of service in production
   </td>
  </tr>
  <tr>
   <td>Elaborate complex automated tests strategies to recreate complex technical issues (race conditions, scalability, multi-threading, memory leaks …)
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Platform and Product
   </td>
   <td>Challenge business teams requirements and bring solutions they didn't imagine
   </td>
  </tr>
  <tr>
   <td>Challenge under-performing existing solutions / processes adopted by business teams
   </td>
  </tr>
  <tr>
   <td>Imagine, design and implement solutions to achieve a product goal with the help of product managers
   </td>
  </tr>
  <tr>
   <td>Create strategies to implement an epic with the help of other experienced engineers and product managers
   </td>
  </tr>
  <tr>
   <td>Actively contribute to maintain a clean functional and technical architecture for the product
   </td>
  </tr>
  <tr>
   <td>Identify product weaknesses and recurring pain points and imagine solutions to solve them
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Business
   </td>
   <td>Participating in identifying solutions to meet product OKRs
   </td>
  </tr>
  <tr>
   <td>Identify how the product can limit Malt’s growth and scaling and imagine solutions to overcome these limitations
   </td>
  </tr>
  <tr>
   <td>Can identify the ROI of solutions and help product managers and their teammates taking wise priorisation decisions
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Team work
   </td>
   <td>Starts being visible in the community (tech, UX etc...) to make the company more visible (presence in meetups, limited interventions)
   </td>
  </tr>
  <tr>
   <td>Participate to recruiting / assess technical skills of candidates
   </td>
  </tr>
  <tr>
   <td>Coach and mentor several other members
   </td>
  </tr>
</table>





## The “engineering leader” ladder

First of all, engineering leaders are technical experts able to individually reach all the individual contributors’ expectations and do it on a daily basis. In our organisation, an engineering leader can be part of a regular team and contribute to its success as any other team member.

But … in addition, engineering leaders federate and coordinate people to achieve a common goal that will benefit the company over the long term. To achieve this, engineering leaders are able to:



* communicate on the goal they want to achieve,
* create motivation around them,
* create alignements and facilitate collective decision making,
* coordinate efforts,
* be accountable all over this process.

While individual contributors can be successful alone, leaders are successful when other people succeed with them.

While individual contributors act in predefined frameworks (their team, a guild, a product backlog, a project plan, existing processes) leaders set up new frameworks, new practices and new organisations to reach a goal that could not be reached individually.

Engineering leaders think strategically on technology, product, organisation … and they bring solutions for Malt’s sake.

![alt_text](./images/IC-leader.png "career path leadership")


The table below shows the big areas where engineering leaders will have to demonstrate their skills.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Impact & ownership
   </td>
   <td>Defines the level of impact engineering leaders have on Malt’s success.
<p>
This is directly correlated to the area of responsibility / ownership engineering leaders endorse in their day-to-day work, the subjects they lead and the impact of the decisions they take or help to take.
<p>
This is also directly linked to how engineering leaders take decisions, how they are able to prioritise work to maximise their impact, and how they monitor success.
   </td>
  </tr>
  <tr>
   <td>Mentorship
   </td>
   <td>Defines how engineering leaders influence positively other members in their own career evolution.
   </td>
  </tr>
  <tr>
   <td>Engineering culture & methodology
   </td>
   <td>Describes how engineering leaders are promoters of our engineering culture in their day-to-day work and behaviour.
<p>
Promoter / Defender of our engineering culture
<p>
Lead by example
<p>
Work on attractiveness
<p>
Quality
<p>
Feedback
   </td>
  </tr>
</table>


Whatever their level in this ladder, an exemplary behaviour and mindset is expected from engineering leaders:



* **Avoid “us versus them” **: we are one team and engineering leaders are expected to act as catalysts to maximise Malt’s results and success. \

* **Aim to improve**: being a good leader requires a good deal of humility. This is applicable personally and collectively. Leaders are expected to keep a learner posture and be obsessed by improving and make the whole organisation improve as well. \

* **Focus on what is actionable: **leaders are expected to spend their time and effort to find actionable solutions to problems. It implies two things:
  * do not keep on looping on problems without bringing solutions,
  * do not bring unsustainable / unrealistic solutions to problems.** \
    **
* **Disagree and commit:** leaders will usually bring strong opinions with them, however keeping strong positions can become counterproductive at some point. Engineering leaders are expected to give their opinion and explain the rationale behind; when no compromise is made and it’s time to take strong decisions, leaders should be able to disagree but commit to apply the decision taken with willingness to make this decision succeed.


### Lead Software engineer

This level is equivalent to Senior Software Engineer 2 for people taking the responsibility of organising and facilitating the work of a group of software engineers. It can be either as a guild lead (leading and organising a community of practice) or as a tech lead of a specific part of the product.

Being guild lead and tech lead is a role in our organisation, not a level in our career path, but someone being constantly successful in one of these roles is a good candidate to evolve in the engineering leadership ladder and become a staff engineer. This position in the career path is a good starting point for future leaders.

It is not a promotion to reach this level from Senior Software Engineer 2 but this level must be reached by people willing to reach the staff engineer level.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Impact & ownership
   </td>
   <td>Lead engineers have a positive impact on the team they work with by bringing solutions to their problems. They are able to communicate these solutions and enable their teammates in implementing these solutions.
   </td>
  </tr>
  <tr>
   <td>They help the team to focus on the right things to do to achieve product goals, reach company key results or minimise product debt. They are able to keep the right balance between tech and product investment for their team.
   </td>
  </tr>
  <tr>
   <td>Able to create a strategy over several weeks to achieve an objective, address pain points.
   </td>
  </tr>
  <tr>
   <td>Able to say “no” for good reasons. They are able to drive their decisions based on metrics.
   </td>
  </tr>
  <tr>
   <td>Identified and recognised as experts in their field (either the discipline of the guild they lead or the domain of the product their team work on), so that people outside the team don’t hesitate to reach out to them to get answers or help.
   </td>
  </tr>
  <tr>
   <td>Be a transparent communication proxy (communicate outside the team, give visibility to other teams, synchronise with other teams ...)
   </td>
  </tr>
  <tr>
   <td rowspan="7" >Mentorship
   </td>
   <td>Let team members benefit from their experience and expertise by answering questions, point to existing best practices or documentation
   </td>
  </tr>
  <tr>
   <td>Be a key person to orchestrate newcomers technical onboarding and ramp-up on the scope of their team
   </td>
  </tr>
  <tr>
   <td>Help individual contributors progress by providing constructive and actionable feedback, either in code reviews or other peer feedback tools.
   </td>
  </tr>
  <tr>
   <td>Organise knowledge sharing sessions: presentation, pair / mob programming sessions within the team 
   </td>
  </tr>
  <tr>
   <td>Assist engineering managers in finding areas of progress for individual contributors in the team.
   </td>
  </tr>
  <tr>
   <td>Start becoming a key person in our hiring process, being able to lead a technical interview with candidates and being able to identify the level of candidates in this career path
   </td>
  </tr>
  <tr>
   <td>Let enough space to senior software engineers to let them take responsibility and ownership  
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Engineering culture & methodology
   </td>
   <td>Demonstrates willingness to help others in case of issue. 
   </td>
  </tr>
  <tr>
   <td>Act as a problem solver more than a detractor or problem spotter.
   </td>
  </tr>
  <tr>
   <td>Be a frequent speaker in our internal events, like Malt Tech Days. Frequently propose new subjects for our internal architecture call / meeting.
   </td>
  </tr>
  <tr>
   <td>Start contributing to our team’s attractiveness (tech blog, social network presence, good candidates referral) 
   </td>
  </tr>
</table>


New comers rarely join Malt at this level as it requires a strong business background, as well as a strong knowledge of our organisation to succeed.


### Staff Software Engineer

At this level, software engineers are really engaged in the leadership ladder and somehow start behaving as a small company CTO with a 360 degrees vision on subjects.

In their day-to-day work, it is not mandatory for staff software engineers to take the role of tech lead nor guild lead even though they could successfully take this role. When working in a team with a tech lead, staff software engineers are expected to support them and help them succeed and be exemplary contributors for the rest of the team and focus on impactful initiatives.

From pure technical skills, we expect staff software engineers to have at least two strong areas of expertise and have an in-depth understanding of how most of Malt’s technical stack actually works.

Staff software engineers are expected to start having positive influence over a full tribe or product line and in many occasions to the whole engineering or product team.

This level can be reached after 1,5 or 2 years as a successful Senior 2 or Lead software engineer including a successful experience as tech lead. Promotion to this level requires entering a dedicated process, including a hype doc as well as a presentation to a promotion committee composed of teammates as well as Malt executives.

When hired externally, we expect staff engineers candidates to have more than 10 years experience in software engineering. We’ll particularly value



* strong previous experience in a tech company, especially marketplaces,
* former CTOs or start-up tech founders, who already has developed a large sense of ownership,
* Former long-time freelancers in tech.

<table>
  <tr>
   <td>
Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Impact & ownership
   </td>
   <td>Staff software engineers will constantly work on identified success metrics, measure these metrics, communicate results and try to improve results.
   </td>
  </tr>
  <tr>
   <td>Staff engineers will think wider than their specific use case and design universal and reusable solutions. They are able to communicate these solutions and enable the whole engineering team to apply them.
   </td>
  </tr>
  <tr>
   <td>They are able to create a strategy over a quarter to reach their goals and orchestrate this strategy over several teams.
   </td>
  </tr>
  <tr>
   <td>They can successfully bootstrap a new strategic product or a complex feature spanning over a whole tribe or the whole product, and design an applicable solution at scale.
   </td>
  </tr>
  <tr>
   <td>Staff engineers act with ownership of Malt's business. They will anticipate failures or proactively find countermeasures to major operational risks.
   </td>
  </tr>
  <tr>
   <td>Staff engineers are able to lead a buy or build discussion, thinking of maximising return on investment and long term maintenance. They are able to drive budget and challenge their decision as business grows.
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Mentorship
   </td>
   <td>Staff engineers are able to engage with individual contributors on a long-term mentorship program, accompanying their mentee to improve or gain new skills.
   </td>
  </tr>
  <tr>
   <td>They are able to onboard newcomers on transversal topics, large architecture pieces or practices. Explain historical decisions in a positive way.
   </td>
  </tr>
  <tr>
   <td>Staff engineers are able to consolidate best practices at the engineering team level, promote them and make sure they are followed everywhere with the help of tech and guild leads.
   </td>
  </tr>
  <tr>
   <td>Be a key person in our hiring process, imagine new technical assessment formats or exercises. They are able to participate in other leaders' recruitment by assessing their technical skills as well as reasoning skills.
   </td>
  </tr>
  <tr>
   <td rowspan="3" >Engineering culture & methodology
   </td>
   <td>Act as a guardian of our engineering culture. Do not hesitate to spot problematic behaviour in teams and act accordingly.
   </td>
  </tr>
  <tr>
   <td>Regularly contribute to our team’s attractiveness (tech blog, social networks …)
   </td>
  </tr>
  <tr>
   <td>Be a frequent speaker in our internal events, like Malt Tech Days. Frequently propose new subjects for our internal architecture call / meeting.
   </td>
  </tr>
</table>


Evolution from staff to principal engineer can be a long and intense journey, as the step can be high. It is possible to value good staff engineers as senior staff engineers. It is not considered as a step up in Malt’s levels but a way to recognize and value staff engineers' work and positive impact.


### Principal Software engineer

Principal software engineers are part of the Product leads group as well as Malt executives (malt leaders). They are involved in strategic decision making for the product team and their voice count in the whole company.

Principal software engineers can follow and lead a full tribe of 6+ squads and be the counterpart of a product director and a director of engineering for the tribe. They behave as a CTO for the tribe, overseeing technical directions and choices while still being hands-on and bring their expertise to the team.

This level can be reached after several years as a successful Staff engineer. Promotion to this level requires entering a dedicated process, including a hype doc as well as a presentation to a promotion committee composed of teammates as well as Malt executives and C-levels.

When hired externally, we expect principal engineers candidates to have more than 10 years experience in software engineering with strong previous experience in a tech company. This level particularly fits well former CTOs of smaller companies or staff engineers or architects in large companies. In their hiring process, candidates will probably meet investors or a panel of C-level executives.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td rowspan="4" >Impact & ownership
   </td>
   <td>Principal software engineers contribute to defining product and / or platform OKRs yearly with other members of the product leads group and are accountable to reach these OKRs. They influence initiatives and roadmaps for Malt’s benefits.
   </td>
  </tr>
  <tr>
   <td>They lead large scale transversal projects and orchestrate large technical changes across the engineering team.
   </td>
  </tr>
  <tr>
   <td>They are good partners for business teams and positively influence these teams’ efficiency by bringing and promoting solutions.
   </td>
  </tr>
  <tr>
   <td>They act as a CTO for a whole product line if needed, being accountable for the technical choices taken by the engineering teams.
   </td>
  </tr>
  <tr>
   <td rowspan="2" >Mentorship
   </td>
   <td>They are expected to mentor and guide leads and staff engineers in their progression.
   </td>
  </tr>
  <tr>
   <td>They can also participate in mentoring product managers and designers.
   </td>
  </tr>
  <tr>
   <td rowspan="2" >Engineering culture & methodology
   </td>
   <td>Convince top level people to join the company
   </td>
  </tr>
  <tr>
   <td>Be a frequent speaker in external events.
   </td>
  </tr>
</table>



### Fellow Software engineer

As there can be only one CTO, this level can be used as a placeholder for people who could take the CTO position while there is already a CTO in place.

Rare are people able to reach this position at Malt. We would expect these persons to have a strong impact on the freelancing market and leverage their technical expertise to make Malt shine in this market.


### CTO

Part of C-level executives at Malt, the CTO is responsible for main technical directions as well as product strategy along with the CPO.


# 


## The “engineering manager” ladder

At Malt, we apply craftsmanship precepts to engineering management. As a consequence being engineering manager is a fulltime job and we don’t expect managers to mix people management with coding, architecture or technical strategy. However, it does not mean engineering managers don’t have impact.

While engineering leaders create impact from their technical expertise and technical strategy, engineering managers create impact on focusing on people, organisation and business.

From a general standpoint, we expect engineering managers to have a strong background as software engineers or technical experts in a specific area (SRE, security, data for instance). As a prerequisite engineering managers at Malt are expected to have spent several years as software engineers or technical experts before being engineering managers.

Progression in the engineering manager ladder is really close to progression in the engineering leaders ladder described above in this document. While stepping up in this ladder, engineering managers are expected to improve the intensity of their impact as well as their area of influence. It requires gaining knowledge and skills as for any other job.

![alt_text](./images/IC-EM.png "Engineering manager's career path")


The table below shows the big areas where engineering managers will have to demonstrate their skills.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Individual management & hiring
   </td>
   <td>Engineering managers follow software engineers in their progression in the career path by providing feedback and guidance.
<p>
They successfully orchestrate performance assessments, leveraging information, peer feedback and facts collected during a period, compile and share assessments to employees and turn them into actionable items and follow action plans over time.
<p>
Engineering managers play a crucial role in our hiring process, in charge of assessing candidates’ ability to integrate in the team, estimate their starting level in the career path and their potential evolution at Malt. 
<p>
Engineering managers orchestrate and optimise the hiring process, creating an efficient relationship with Malt’s talent acquisition team. They convey an attractive image of Malt to candidates and act as Malt’s ambassadors in their exchange with external people.
<p>
Engineering managers shape their team to face future business and product challenges. They build efficient teams composed of people with the right skills at the right place.
   </td>
  </tr>
  <tr>
   <td>Team efficiency
   </td>
   <td>Based on their past experience, their research and teams’ daily work, engineering managers help teams collectively maximise their impact.
<p>
When teams struggle to deliver and perform, engineering managers bring support, good questioning and solutions to discuss with the team. They help teams find their best organisation and processes. They compile and present key performance metrics for teams and encourage teams to improve these metrics.
<p>
Engineering managers structure and coordinate projects, ensuring they are delivered in good conditions for product’s quality, Malt’s business and people in the team.
<p>
At some points, engineering managers can coordinate cross-teams initiatives or programs.
   </td>
  </tr>
  <tr>
   <td>Business 
   </td>
   <td>Engineering managers are software industry experts. They know how to build and deliver high quality software and how to make people and teams successful in this mission over the long term. They can help teams manage priorities to maximize their short term impact as well as keeping a viable product long term.
<p>
Engineering managers should bring a 360 vision to their teams, covering technical, product, business and people matters.
<p>
Engineering managers working with platform teams (as Cloud operations, security, data engineering) must have a strong background and culture on these topics. They are not expected to be hands-on themselves but must be knowledgeable on these subjects and give meaningful advice to their teams. 
<p>
Engineering managers are expected to become business experts on Malt’s topics. They understand complex business matters and can make decisions based on business. They are able to evaluate impact in terms of business for Malt.
<p>
They cultivate business-centricity in their team.
<p>
They communicate on company goals and strategy. While stepping up in their career, they participate in goals and strategy definition for the product and the company.
   </td>
  </tr>
  <tr>
   <td>Culture & engineering organisation
   </td>
   <td>While our engineering culture is everybody's subject in the team, engineering managers are the guardians for this culture. They detect behaviours that could harm our culture and react accordingly with positive and inclusive communication.
<p>
Engineering managers are expected to create good trust relationships with and between engineers. They create transparent and frictionless communication with people and teams.
<p>
Culture is also a matter of processes and tooling. Engineering managers are expected to be key contributors in order to create replicable and efficient internal processes, document these processes, measure their efficiency and ensure they are followed by people in the team.
   </td>
  </tr>
</table>


As engineering leaders, engineering managers must have an exemplary behaviour and mindset for Malt.



* **“Size doesn't matter but impact does”** - number of engineers under management is not a success metric for engineering managers at Malt. Keeping people while they’d rather evolve and move to another team is a losing battle. There is no need to fight for headcount, instead engage the battle for impact with your team.
* **“Courageous managers”** - Malt’s management internal training course is called “courageous manager”. Many management mistakes usually made by junior managers come from lack of courage and transparency. Hiding their difficulties to people won’t help them and usually prevent them from improving.
* **“Celebrate success collectively”**  - being an engineering manager requires a good dose of humility. Don’t take the credits for your teams’ success, instead celebrate their successes with them and question yourself when they fail.
* **“Feel accountable for the company”** - engineering managers are ambassadors of the company’s culture in the engineering team. They create bridges between people and teams, they act and behave for Malt’s sake.


### Engineering manager

This level is the starting point of an engineering manager career at Malt.

Internally, it is accessible to senior software engineers 1 or 2, or lead software engineers willing to focus on people management.

When hired externally, this level is accessible to people with a strong background in software engineering (at least 6 years experience as software engineer or related) and willing to focus on people management.

Engineering managers are assigned to 1 to 3 squads, working on a consistent scope in the product team (from 5 to 12 engineers approximately). Engineering managers are responsible for the delivery of their teams, their performance and their choices and decisions over the long term.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Individual management & hiring
   </td>
   <td>Engineering managers master all basic people management skills and practices at Malt. They know and apply internal human resources processes and policies.  
<p>
Engineering managers are expected to create good and fluid day-to-day individual relationships with people in their team, meeting them on a regular basis.
<p>
Engineering managers are able to adapt their management style to different situations and act accordingly.
<p>
They engage people of their team in Malt’s career path, orchestrating performance assessments, peer feedback sessions and individual objectives follow-up. They manage newcomers' probation periods and guide them in a successful initial on-boarding.
<p>
Engineering managers master the individual contributor’s ladder. They can easily identify areas of improvements for individual contributors and how to make them improve and step up. They create good conditions for people’s success in the IC ladder.
<p>
Engineering managers are comfortable with the hiring process and can lead a “round 1” interview and give an overall appropriate feedback about candidates and how to position them in the individual contributor’s ladder.
<p>
Engineering managers can manage engineering leaders and may require advice and help to hire and make leaders grow in their career.
   </td>
  </tr>
  <tr>
   <td>Team efficiency
   </td>
   <td>Engineering managers create conditions for continuous improvement. They organise required meetings or events that will help the teams succeed collectively (retrospectives, workshops, offsites …). During these collective events, engineering managers come with ideas, suggestions, questions, they propose appropriate activities and tools to favour collective work.
<p>
Engineering managers can help their team organise their delivery backlog, helping them prioritise and take decisions by bringing a 360 degrees vision.
<p>
Engineering managers are ambassadors of their teams’ pain points and feedback. They help their teams find solutions to friction they could have with other teams. They create success conditions between teams and help them coordinate when needed.
<p>
They master basic engineering performance metrics, their meaning and how to capture them. They make teams own these metrics, being transparent and good coaches for teams.
<p>
Engineering managers shape harmonious teams with complementary skills and personalities.
   </td>
  </tr>
  <tr>
   <td>Business 
   </td>
   <td>Engineering managers understand and are knowledgeable in the functional scope of their teams. They fully understand the product their teams build, how it contributes to Malt’s success and how it interacts with other parts of the product.
<p>
They fully understand the needs of end-users their teams serve and are able to give their teams more general context about these needs. They are recognised by product managers they work with as knowledgeable product people and can easily talk about product vision and current challenges to someone else.
<p>
They fully understand how the product their teams build is used internally by business teams. They are aware of the interactions their teams need to create with business teams. They can value these interactions at their right level and encourage their teams in collaborating with business teams.
<p>
Engineering managers are expected to perfectly know company and product OKRs and how their teams actually contribute to these OKRs. 
   </td>
  </tr>
  <tr>
   <td>Culture & engineering organisation
   </td>
   <td>Engineering managers are expected to apply Malt’s culture on a daily basis.
<p>
They actively contribute to improving processes and tooling in place in the engineering team by taking tasks in the common engineering management backlog. They act on these tasks with ownership as engineers do in their development tasks. They communicate to the group transparently on these tasks' status. They are able to orchestrate small changes at the engineering team level.
   </td>
  </tr>
</table>



### Senior Engineering Manager

This is the confirmation level for engineering managers. Reaching this level does not affect the scope of engineering managers, as senior engineering managers remain assigned to 1 to 3 squads (from 5 to 12 engineers approximately) and keep approximately the same daily job. However senior engineering managers are expected to have gained more experience, have worked on more impactful subjects and take more initiatives at a larger scale.

Internally, it is accessible to engineering managers after 1,5 to 2 years.

When hired externally, this level is accessible to people having a significant (at least 2 years) engineering management experience in a tech / product company or people with a strong successful entrepreneurial background or executive position in a smaller company.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Individual management & hiring
   </td>
   <td>Leveraging their experience, senior engineering managers create more impactful relationships with the engineers in their teams.
<p>
They are able to better detect weak signals and better anticipate future issues, like demotivation, frustration, slow individual performance drops.
<p>
Senior engineering managers master the engineering leadership ladder of the career path. They are good mentors for engineers willing to reach either leadership or management levels in the career path. They will positively challenge them to maximise their impact for Malt’s sake.
<p>
Senior engineering managers orchestre engineering leaders recruitment. They will also be key contributors in recruiting other managers.
<p>
They also motivate rare profiles to engage in the hiring process.
   </td>
  </tr>
  <tr>
   <td>Team efficiency
   </td>
   <td>Leveraging their experience, senior engineering managers will improve their positive impact on their teams’ ability to deliver efficiently.
<p>
At this stage, senior engineering managers start thinking in a more systematic way of replicating practices outside their team. They consolidate their teams’ processes, making them applicable to other teams and even externally.
<p>
Senior engineering managers also reach another step in terms of cross-team coordination and collaboration skills. When required, they can coordinate the delivery of a cross-team project or initiative along with product managers and tech leads. They can play the role of small program manager demonstrating advanced project management and coordination skills.
   </td>
  </tr>
  <tr>
   <td>Business 
   </td>
   <td>As engineering managers, senior engineering managers are expected to be business experts.
<p>
They extend their business expertise beyond the scope of their teams.
   </td>
  </tr>
  <tr>
   <td>Culture & engineering organisation
   </td>
   <td>Senior engineering managers are expected to be key contributors to the engineering culture and organisation suggesting processes and tooling improvements that apply to the whole engineering team. They can manage larger initiatives in autonomy and orchestrate change management.
   </td>
  </tr>
</table>



### Engineering Director

As principal software engineers, engineering directors are part of the Product leads group as well as Malt executives (malt leaders). They are involved in strategic decision making for the product team and their voice count in the whole company.

Expectations at this level really depend on the scope it applies to. At Malt, we have two variations of engineering directors.


#### Engineering directors in product tribes

For product tribes, an engineering director can be in charge of organising a full tribe when it reaches a large size (starting at 6 squads and 3 engineering managers). In this case, the engineering director collaborates with the corresponding product director to lead this tribe as a small product team, or a product line. In this case, the engineering director role is really close to the role of VP engineering at a smaller scale.

This is the first level of manager of managers in the career path. Engineering directors are expected to be good mentors for managers and support them in their job.

Engineering directors in product tribes are expected to develop a sharp long-term strategic vision for their tribe, create yearly hiring plans along with the VP engineering and consolidate required budgets.

At the time of writing this document, there is no engineering director in product tribes as Malt hasn’t reached the necessary size yet.


#### “Head of” roles

Engineering directors can also be positioned as “head of” transversal topics for the whole product team or even the whole company (e.g. head of cloud operations, head of data, head of security …).

In this case, engineering directors are expected to be subject experts with strong organisation, strategic and people management skills. They are fully responsible and accountable on the scope they lead.


<table>
  <tr>
   <td>Area
   </td>
   <td>Description
   </td>
  </tr>
  <tr>
   <td>Individual management & hiring
   </td>
   <td>Engineering directors positioned as “head of” are expected to reach the same level of maturity as senior engineering managers on individual management.
<p>
Beyond that, they can create specific implementations of career paths in their speciality, list and detail specific jobs, corresponding expectations and make sure it is aligned with the market.
<p>
Engineering directors positioned as “head of” orchestrate the hiring process for their speciality.
   </td>
  </tr>
  <tr>
   <td>Team efficiency
   </td>
   <td>Engineering directors positioned as “head of” are in charge of organising operations related to their speciality along with their team. 
<p>
They decide the success metrics along with their teams and the VP engineering and orchestrate these metrics follow-up and reporting. They act as senior engineering managers do on their teams.
<p>
They manage the budget affected to their teams, software licensing and subscription, as well as negotiating contracts with providers.
   </td>
  </tr>
  <tr>
   <td>Business 
   </td>
   <td>Engineering directors positioned as “head of” are experts in leading the business of their speciality.
<p>
They are also expected to be experts in Malt’s business to provide enough context to their teams and make appropriate decisions.
<p>
They understand Malt’s business and financial model to work on maximising ROI.
   </td>
  </tr>
  <tr>
   <td>Culture & engineering organisation
   </td>
   <td>Engineering directors positioned as “head of” apply our engineering culture to their specificiality. They create specific processes matching their operational requirements and make sure they are transparently shared with the rest of the organisation and still fully aligned with the rest of the engineering team.
<p>
They are in charge of promoting their speciality externally and thus contribute to the engineering team’s attractiveness. 
   </td>
  </tr>
</table>


At this step, it is possible to promote successful engineering directors to “senior engineering directors” in order to recognise their success and experience in this position.


### VP engineering and Senior VP engineering

In charge of the whole engineering team, making the organisation scale and able to address future challenges. Engineering career path writer :-)
