---
title: Organization
subtitle: 
sidebar: true
breadcrumbs:
    - label: "Handbook"
      url: "/pages/handbook"
---

## About

Our organization reflects the values listed [here](/pages/values-culture) in the sense that they have the goal to create empowered teams with a large autonomy. 


![alt_text](./images/organization.png "Organization overview")


Our product is split into big functional areas reflecting our business and fully under the responsibility of **Tribes**. 
The first part of this document introduces tribes, their mission, their responsibilities, the foundations of their internal organization and how they can collaborate together. 
The main objective of this setup is to create product ownership.

Putting the right focus on raising our expertise level requires dedication and ownership. 
These subjects are addressed collectively by **Guilds**, active communities of practice gathering people of the product team. 
This document shows how guilds work and are driven.

There could not be a product organization without a team in charge of foundations and shared services, what we call **the Platform Team** here at Malt. 
This document describes the scope of this team and how it collaborates with other teams.

To be successful, our organization has to be adaptable to changes. 
This document describes how we can address transversal or very specific investment projects introducing the concept of **temporary squads**.

Finally, this is a living organization and this document describes how the organization breathes in time.




## Tribes 

A tribe can be seen as a mini startup in Malt.

![alt_text](./images/tribes.png "Tribes overview")


We defined the tribes by splitting the product into high-level, immutable functional areas with


* Well-defined scope, set of functionalities, boundaries and contracts
* Specific ambitions, challenges and KPIs
* Identified business partners in the company
* Align tech architecture as much as possible

We aim to have a dedicated tribe to operate each functional area

* Multidisciplinary team (business partner, product manager, data analyst, product designer, developers, data scientist ...)
* Make the team fully responsible from A to Z: support, feedback, enhancements, bugs, projects, technical debt, KPIs monitoring, quality of service ...

A tribe has a constant perimeter and delimited boundaries. When the product team is growing, the tribe is splitted into more squads with a specific scope for each. 
For example, a Tribe “Clients” can be splitted into different personas (SMBs, MidSize, Corporate) or topics (payment, invoicing, sourcing).



> Note: What is not a tribe ?
* A temporary team working on a project
* An empty shell without any owner 
* A team working only on technical or design topics. 

### Team responsibilities 

The Tribe is fully responsible from A to Z of its scope. Here is an overview of these responsibilities:

* Address security and quality of service issues in their scope
* Manage support long term, fix issues...
* Answer feedback
* Maintain product knowledge & documentation
* Promote internally the product and ensure that the external communication is working (with marketing and communication team)
* Support its business partners: for example go to a client meeting to sell the product, help on a call for tender, participate in a community event as an attendant or a speaker, etc.
* Monitor critical KPI (conversion funnel, acquisition, churn, etc.) and alert the top management if needed
* Code ownership: quality, performance, technical debt ...
* Product discovery & User research: the tribe is the expert and the voice of its users.
* Auto-organization: If needed define or adapt some process for its own needs


### How the Tribes work & Roles 

As stated above, tribes own their part of the product from A to Z, as a mini startup would own his own product.

They are fully responsible for discovering and defining new solutions to make their business grow: it’s what we call the **discovery phase**. 
This phase is orchestrated by the Product Manager (PM) in collaboration with Product Designers, Software engineers and Data Analysts. 
They are all in charge of organizing their work to find the best area of improvement for the product considering users requirements and Malt’s priorities. 
For a given solution, this phase ends when the team has converged into a clear and well defined solution.

To make it clear, everyone cooperates but at there is 4 areas to cover in discovery: 
* product managers are responsible for viability and value
* product designers are responsible for usability
* software engineers are responsible for feasibility

To make these dreams come true, the **delivery phase** consists in developing and delivering the solution. 
It is mainly led by the Tech Lead (TL) with the help of the rest of the team. This phase ends when the solution is fully delivered and activated in production

This is inspired by the double-diamond approach.


![alt_text](./images/double-diamond.jpg "Double diamond approach")



> Note: At first sight, this section could let the reader think we are working in waterfall mode, with long design phases followed by long implementation phases and big tunnel effects. 
The double-diamond approach does not actually forbid being agile, delivering iteratively and focusing on value first. 
So, no doubt, we are not IBM ;-)


Tribes being product-centric organizations (and not project-centric), they cannot only focus on building new solutions, they also have to maintain and improve existing solutions. 
The maintenance phase is led by the Tech Lead on all technical aspects and the Product Manager on all knowledge and communication aspects.


### Inter-tribes collaboration

Though tribe's scopes are clear and their boundaries are identified, there will always be situations where a tribe needs the help of another to design and implement a solution. 
These situations should be clearly identified during the discovery phase and should be anticipated.

In these situations, we will always try to avoid dependencies between tribes and opt for either a “do it yourself” or “we can help you” approach.

In the “do it yourself” scenario, both tribes will co-design the changes to perform and the requesting tribe (the one having a problem to solve) will perform all the 
required code changes under the control of the other tribe (the one owning the code to change).


![alt_text](./images/doityourself.png "Do it yourself approach")


In the “we can help you” approach, the requesting tribe will on-board a contributor of the other team for a period of time in order to bring his expertise to the tribe.

These situations should rarely occur. If they tend to occur too often, it should be brought to management as an organization issue.

> Note: in any case, the solution should not be "we don't have any time for you"


If the topic is a big one and would require a dedicated staffing from both tribes, refer to [Temporary Squads](#temporary-squads)


## Guilds 

Having mini startups working independently and autonomously on their specific topics can become underperforming if there is no alignment. 
Long term, it would lead to an heterogeneous architecture, user experience, tooling … rebuilding solutions differently several times for the same problem. 
We don’t have time and resources to waste but need to serve our ambition and keep on investing wisely.

Guilds are best-practices communities gathering people with the same expertise area with the objective to:

* **raise the level up** on their expertise area in the Malt product team,
* bring **new knowledge** to the Malt product team,
* come up with **aligned solutions** on common problems, promote them and make sure they are implemented the same way everywhere in the product when it makes sense
* monitor and question these solutions long term.


### Guilds roles 

To make the magic happen, it requires time and focus from guilds’ participants. 
That’s why each guild is led by a **Guild Lead** whose first responsibility is to make the guild reach the objectives described above on its specific expertise area. 
Being a Guild lead is at least a half-time job and that’s why Guild Leads cannot be part of a Tribe full time but belong to the platform team for their background job. 
In addition to their work on guilds, Guild Leads contribute to the platform team task force - especially contributing to our Cloud and Data infrastructures or 
implementing and maintaining reusable pieces of code. 
Guild leads are warmly welcomed to join a tribe or a temporary squad for a given period of time to help on a specific subject, 
this situation should not last more than 2 weeks and should not occur more than twice a quarter per lead.

**Guild participants** are members of the product team interested in the guild’s expertise area and keen to actively participate in its mission. 
Guild participants are part of Tribes in their day-to-day life and should keep mainly focused on their tribe work. 
Being part of a guild, they should actively take part in guild-specific events, 
communicate guild decisions to their tribe and take some guild tasks back in their tribe's backlog if needed. On the other hand, 
they should give visibility to the Guild Lead on how things are going in their tribe and alert guild members if there is a specific topic to address in the guild’s expertise area.
Tribes must always dedicate about 20% of their work time to guild work.

There are actually 2 kind of guild participants:
* “Occasional participants” who will occasionally attend to guild meetings, contribute to technical watch (share links, news, ideas) or bring specific topics / questions.
* “Active participants” who will regularly attend guild meetings, take action items, tasks and so will actively contribute to the success of the guild. Active participants to guilds are well identified in the guild and in their respective team.

It’s up to the guild to define how members communicate together and with the rest of the product team and the company. 
However, generally speaking, all types of communication should be made public to the company for transparency and better information sharing. 
For instance, guild meetings should be open to other participants (in read-only mode), slack channels should be public only and there should not be private communication about guilds. 
Guilds should issue periodic written information to the rest of the product team to give them visibility of what was done, what will be done.


### Guild responsibilities 

Legend:
* R: Responsible of doing the task
* A: Accountable of the task done (or not done)
* C: Consulted - part of the decision making process
* I: Informed when a decision is made / a task is done


<table>
  <tr>
   <th>Task
   </th>
   <th>Guild lead
   </th>
   <th>Guild participant
   </th>
   <th>Management (CTO, VPE, Head of ...)
   </th>
   <th>Other product team members
   </th>
  </tr>
  <tr>
   <td>Organize guild events / work
   </td>
   <td>R / A
   </td>
   <td>R
   </td>
   <td>
   </td>
   <td>I
   </td>
  </tr>
  <tr>
   <td>Find / study / present new practices or  solutions in the expertise area
   </td>
   <td>R / A
   </td>
   <td>R
   </td>
   <td>I
   </td>
   <td>I
   </td>
  </tr>
  <tr>
   <td>Decide a solution / practice should be used
   </td>
   <td>R / A
   </td>
   <td>R
   </td>
   <td>C / I (see ADR process)
   </td>
   <td>I
   </td>
  </tr>
  <tr>
   <td>POC of a solution
   </td>
   <td>R / A
   </td>
   <td>R
   </td>
   <td>I
   </td>
   <td>I
   </td>
  </tr>
  <tr>
   <td>Implement solutions in the product
   </td>
   <td>C / I
   </td>
   <td>R / A
   </td>
   <td>
   </td>
   <td>R
   </td>
  </tr>
  <tr>
   <td>Make sure the product keeps being aligned
   </td>
   <td>R / A
   </td>
   <td>R
   </td>
   <td>I
   </td>
   <td>R
   </td>
  </tr>
</table>



### List of guilds 


<table>
  <th>
   <td>Guild
   </th>
   <th>Mission
   </th>
  </tr>
  <tr>
   <td>Backend architecture
   </td>
   <td>Thinks about the architecture of the platform: points out pain points and discusses solutions to correct this architecture. Said pain points may be technical ones (inter-service communication, batches, etc.) or also business/organization ones (how domains of the platform are (not) represented, how our codebase is aligned with them or with our teams).
   </td>
  </tr>
  <tr>
   <td>Frontend
   </td>
   <td><ul><li>Architecture: create and maintain sets of reusable components, services and tools for our different web applications. Promote guidelines to support new practices across tribes.</li>
   <li>Developer eXperience: make sure engineering team members have the proper tools / documentation to be productive.</li>
   </ul>
   </td>
  </tr>
  <tr>
   <td>Security
   </td>
   <td><ul>
   <li>Continuous improvement of the product's security by implementing DevSecOps practices.</li>
    <li>Discuss and address security issues related to the product and organization.</li>
    <li>Anticipate threats, and deploy remediations.</li>
    <li>Share knowledge about security practices, tools, and spread security culture.</li>
    </ul>
   </td>
  </tr>
  <tr>
   <td>Data
   </td>
   <td>
    <ul>
        <li>Lead the data governance</li>
        <li>Guarantee of high level of data quality</li>
        <li>Build efficient data stack for Data science and data Analysts</li>
    </ul>
   </td>
  </tr>
  <tr>
   <td>Design system and UX
   </td>
   <td>Facilitate the conception and development phases by creating a set of standards, reusable components and patterns.
   </td>
  </tr>
  <tr>
   <td>User Research
   </td>
   <td>Allow the teams to be autonomous in terms of process and tools.
<p>
Support the teams regarding big projects
   </td>
  </tr>
  
</table>

## Platform and business scale

To be successful, tribes need to rely on a solid platform on top of which they can build, deploy and run the applications and services they create and maintain.

A part of the engineering team is dedicated to creating and maintaining such a solid platform. 
This group is composed of cloud engineers, software engineers, data engineers and all the guild leaders (as described above).

They are responsible to maintain and continuously improve:

* our **Cloud infrastructure** (kubernetes, databases, storage, datadog ...)
* our **Data infrastructure** (ETL, AirFlow, BigQuery ...)
* our **continuous integration and delivery infrastructure** (Gitlab CI, ArgoCD …)
* some **shared code and services** consumed by the other teams and that could not naturally be owned by a specific tribe
* some tools to improve **Malt’s business teams efficiency**.

As this team gathers all the guilds leads, it will be the main guarantor of tribes alignment on a shared technical vision and code organization.

> Note: What is not the platform team:
* an architecture team that would decide everything
* a team where people work on fancy technical subjects whereas other teams work on the legacy stuff (quite the contrary :-))
* a privileged team where only "the best developers" can come


## Temporary Squads 


### Cross-tribes projects

Regarding the company priorities, we may consider to start big cross tribes projects or focus. 

In terms of organization, this squad will be staffed with people from all tribes concerned, depending on their domains of expertise and availability.


### Mono-tribes projects

We may also start big projects with a high priority for the company that are linked to only one Tribe.
In that case, we would create a temporary squad within the tribe with a dedicated focus. 
 
