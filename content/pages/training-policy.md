---
title: Training
subtitle: How Malters stays up to date
sidebar: true
breadcrumbs:
- label: "Life At Malt"
  url: "/careers"
---

At Malt there are several forms of training available because we strongly believe that we have to invest in the people that build our company.

We believe there are several objectives to training:

* Improve Malters’ skills
* Develop personal curiosity 
* Spread knowledge

And here’s what we put in place to get there:

## Brown Bag lunch

Regularly we invite someone, either internally or externally to talk about a specific topic around a lunch.
The goal is to share best practices with an expert. 

And of course, some Malters also share their knowledge with other teams outside the company.

## Malt Tech Days

https://www.youtube.com/watch?v=4RxXlWglK5g

Every 6 months, we organize the Malt Tech Days event: an internal conference where product teams and external speakers can talk about different topics. Among the topics discussed, we have had in the past: 
Hexagonal architecture, API Design, biases in data analysis, capturing the flag, riskstorming, TLA+, Shenandoah, Kafka 

You can learn more about the 2022 edition [on our blog](https://medium.com/nerds-malt/malt-tech-days-product-and-tech-culture-on-steroids-4da9e5ba19eb) page.

And maybe we will open this format up to more people in the future, but sshh, it's a secret :)

## Malt Swap Days

From time to time, we organize a Malt Swap Day with another team from another company. By definition we don’t do it often because it means finding the right people who match our culture and want to spend this time with us. During the day, we create an "unconference": every participant can suggest a topic and we will vote for the best ones. Then, we create several groups and people are split up into those groups to create an open discussion.

## Conferences

Every Malters can attend to the conferences that they are interested in, period.

There is no hassle about having a budget, asking for a hotel room, and paying for the conference (well except if you want to attend a conference in Dubai....)


Of course our teams are also welcomed to talk about stuff they do during conferences! You can find some of them in those conferences.

[Here is a list of the conferences we held in the past](/events).


## Malt Tech Training Sessions

Every month, a special session is organized to train Malters about a specific part of the product. 
This session is recorded and then splitted into several parts in our learning platform to become an asset for future Malters. 

This is not like a conference, the subject has to be specific to Malt. The goal is not to share general knowledge or general best practices but the usage we have at Malt.
It's like a training session to explain how we do something.

## Malt Academy

Malt organizes Malt academies for all freelancers on the platform. And of course it's a great way for Malters to learn things.  
Those academies are recorded and can be found on [our Youtube channel](https://www.youtube.com/channel/UCPA9grRuUqVLyrazhIqd8BQ).


## E-Learning

Last but not least, all Malters are free to use any learning platform they want. Like for conferences, they can attend the ones they are interested in without any hassle and Malt takes care of the rest.

